<?php
/*
  WC-Rio Child theme
  based on 2023 WP theme
 */

function wcrio_fse_setup() {
    add_theme_support( 'wp-block-styles' );
    add_editor_style( 'style.css' );
}
add_action( 'after_setup_theme', 'wcrio_fse_setup' );

function wcrio_css_js() {
	wp_enqueue_script( 'wcrio-js', get_stylesheet_directory_uri() . '/main.js', array( 'jquery' ), 1.1, true );
	wp_enqueue_style( 'wcrio-css', get_stylesheet_uri() );
}
add_action( 'wp_enqueue_scripts', 'wcrio_css_js' );

